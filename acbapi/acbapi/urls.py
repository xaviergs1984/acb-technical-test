from django.contrib import admin
from django.urls import path
from backend.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('acb-api/empty-database', emptyDBView.as_view()),
    path('acb-api/pbp-lean/<int:gameID>', pbpLeanView.as_view()),
    path('acb-api/game-leaders/<int:gameID>', gameLeadersView.as_view()),
    path('acb-api/game-biggest-lead/<int:gameID>', gameBiggestLeadView.as_view())
]
