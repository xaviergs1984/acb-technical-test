from django.test import TestCase
from rest_framework.test import APIClient
from django.contrib.auth.models import User
import base64
from .models import *

gameID_OK = 103789
gameID_ERROR = 999
username = 'acbapitest'
password = 'acbapitest'

class PbPLeanCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username=username, password=password)
        self.user.is_staff = True
        self.user.save()
        self.client = APIClient()
        self.credentials = base64.b64encode(f'{username}:{password}'.encode('utf-8'))
        self.client.credentials(HTTP_AUTHORIZATION='Basic {}'.format(self.credentials.decode('utf-8')))

    def test_get_pbplean_from_matchevents(self):
        # Partido correcto
        response = self.client.get('/acb-api/pbp-lean/{}'.format(gameID_OK))
        matchEventTest = response.data[0]
        self.assertEquals(len(response.data), 510)
        self.assertEquals(len(matchEventTest), 4)
        self.assertIsInstance(matchEventTest['team_id'], int)
        self.assertEquals(matchEventTest['team_id'], 2503)
        self.assertIsInstance(matchEventTest['player_license_id'], int)
        self.assertEquals(matchEventTest['player_license_id'], 30001114)
        self.assertIsInstance(matchEventTest['action_time'], str)
        self.assertEquals(matchEventTest['action_time'], '00:10:00')
        self.assertIsInstance(matchEventTest['action_type'], int)
        self.assertEquals(matchEventTest['action_type'], 599)

        # Partido no existente
        response = self.client.get('/acb-api/pbp-lean/{}'.format(gameID_ERROR))
        self.assertEquals(len(response.data), 0)

    def test_get_pbplean_from_database(self):
        # Partido correcto
        response = self.client.get('/acb-api/pbp-lean/{}'.format(gameID_OK))
        response = self.client.get('/acb-api/pbp-lean/{}'.format(gameID_OK))
        matchEventTest = response.data[0]
        self.assertEquals(len(response.data), 510)
        self.assertEquals(len(matchEventTest), 4)
        self.assertIsInstance(matchEventTest['team_id'], int)
        self.assertEquals(matchEventTest['team_id'], 2503)
        self.assertIsInstance(matchEventTest['player_license_id'], int)
        self.assertEquals(matchEventTest['player_license_id'], 30001114)
        self.assertIsInstance(matchEventTest['action_time'], str)
        self.assertEquals(matchEventTest['action_time'], '00:10:00')
        self.assertIsInstance(matchEventTest['action_type'], int)
        self.assertEquals(matchEventTest['action_type'], 599)

        # Partido no existente
        response = self.client.get('/acb-api/pbp-lean/{}'.format(gameID_ERROR))
        response = self.client.get('/acb-api/pbp-lean/{}'.format(gameID_ERROR))
        self.assertEquals(len(response.data), 0)
    
    def tearDown(self):
        self.client.logout()

class GameLeadersCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username=username, password=password)
        self.user.is_staff = True
        self.user.save()
        self.client = APIClient()
        self.credentials = base64.b64encode(f'{username}:{password}'.encode('utf-8'))
        self.client.credentials(HTTP_AUTHORIZATION='Basic {}'.format(self.credentials.decode('utf-8')))

    def test_get_gameleaders_from_matchevents(self):
        # Partido correcto
        response = self.client.get('/acb-api/game-leaders/{}'.format(gameID_OK))
        self.assertEquals(len(response.data), 2)
        self.assertEquals(len(response.data['home_team_leaders']), 2)
        self.assertEquals(len(response.data['away_team_leaders']), 2)
        self.assertIsInstance(response.data['home_team_leaders'][0]['player_license'], int)
        self.assertEquals(response.data['home_team_leaders'][0]['player_license'], 30002721)
        self.assertIsInstance(response.data['home_team_leaders'][0]['points'], int)
        self.assertEquals(response.data['home_team_leaders'][0]['points'], 13)

        # Partido no existente
        response = self.client.get('/acb-api/game-leaders/{}'.format(gameID_ERROR))
        self.assertEquals(len(response.data), 2)
        self.assertEquals(len(response.data['home_team_leaders']), 0)
        self.assertEquals(len(response.data['away_team_leaders']), 0)

    def test_get_gameleaders_from_database(self):
        # Partido correcto
        response = self.client.get('/acb-api/game-leaders/{}'.format(gameID_OK))
        response = self.client.get('/acb-api/game-leaders/{}'.format(gameID_OK))
        self.assertEquals(len(response.data), 2)
        self.assertEquals(len(response.data['home_team_leaders']), 2)
        self.assertEquals(len(response.data['away_team_leaders']), 2)
        self.assertIsInstance(response.data['home_team_leaders'][0]['player_license'], int)
        self.assertEquals(response.data['home_team_leaders'][0]['player_license'], 30002721)
        self.assertIsInstance(response.data['home_team_leaders'][0]['points'], int)
        self.assertEquals(response.data['home_team_leaders'][0]['points'], 13)

        # Partido no existente
        response = self.client.get('/acb-api/game-leaders/{}'.format(gameID_ERROR))
        response = self.client.get('/acb-api/game-leaders/{}'.format(gameID_ERROR))
        self.assertEquals(len(response.data), 2)
        self.assertEquals(len(response.data['home_team_leaders']), 0)
        self.assertEquals(len(response.data['away_team_leaders']), 0)

    def test_get_gamebiggestlead_from_matchevents(self):
        # Partido correcto
        response = self.client.get('/acb-api/game-biggest-lead/{}'.format(gameID_OK))
        self.assertEquals(len(response.data), 2)
        self.assertIsInstance(response.data['home_team'], int)
        self.assertEquals(response.data['home_team'], 16)
        self.assertIsInstance(response.data['away_team'], int)
        self.assertEquals(response.data['away_team'], 2)

        # Partido no existente
        response = self.client.get('/acb-api/game-biggest-lead/{}'.format(gameID_ERROR))
        self.assertEquals(len(response.data), 2)
        self.assertIsInstance(response.data['home_team'], int)
        self.assertEquals(response.data['home_team'], 0)
        self.assertIsInstance(response.data['away_team'], int)
        self.assertEquals(response.data['away_team'], 0)
    
    def tearDown(self):
        self.client.logout()