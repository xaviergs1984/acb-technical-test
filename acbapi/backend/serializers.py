from .models import *
from rest_framework import serializers

class ApiCallSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApiCall
        fields = '__all__'

class PbPLeanSerializer(serializers.ModelSerializer):
    class Meta:
        model = PbPLean
        fields = '__all__'

class GameLeadersSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameLeaders
        fields = '__all__'

class GameBiggestLeadSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameBiggestLead
        fields = '__all__'