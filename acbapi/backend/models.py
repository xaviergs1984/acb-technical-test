from django.db import models

class ApiCall(models.Model):
    game_id = models.IntegerField(default=0)
    endpoint = models.CharField(max_length=20, default='')

class PbPLean(models.Model):
    game_id = models.IntegerField(default=0)
    team_id = models.IntegerField(default=0)
    player_license_id = models.IntegerField(default=0)
    action_time = models.CharField(max_length=8, default='')
    action_type = models.IntegerField(default=0)

class GameLeaders(models.Model):
    game_id = models.IntegerField(default=0)
    statistic_type = models.CharField(max_length=10, default='')
    home_team_leader = models.IntegerField(default=0)
    home_team_value = models.IntegerField(default=0)
    away_team_leader = models.IntegerField(default=0)
    away_team_value = models.IntegerField(default=0)

class GameBiggestLead(models.Model):
    game_id = models.IntegerField(default=0)
    home_team = models.IntegerField(default=0)
    away_team = models.IntegerField(default=0)