from .models import *
from .serializers import *
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

import os
import collections
import requests
import json

# Devuelve todas las acciones de un partido a partir del endpoint matchevents de la API de la ACB
def getMatchFullEvents(gameID):
    acbAPIToken = os.environ.get('ACB_API_TOKEN', 'ERROR: ACB_API_TOKEN is not defined')
    headers = {'Authorization': acbAPIToken}

    try:
        response = requests.get('https://api2.acb.com/api/v1/openapilive/PlayByPlay/matchevents?idMatch={}'.format(gameID), headers=headers)
        return response.json()
    except Exception as e:
        print(acbAPIToken)
        return None

# Vacía el contenido de todas las tablas de la base de datos
class emptyDBView(APIView):
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        ApiCall.objects.all().delete()
        PbPLean.objects.all().delete()
        GameLeaders.objects.all().delete()
        GameBiggestLead.objects.all().delete()
        return Response('OK')

# Endpoint Play by Play Lean
class pbpLeanView(APIView):
    # Autenticación básica
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        gameID = self.kwargs['gameID']
        # Consulta si se ha accedido previamente a ese mismo endpoint y partido
        repeatedCall = ApiCall.objects.filter(game_id=gameID, endpoint='pbp-lean').count() > 0
        matchEvents = []

        if not repeatedCall:
            # En caso de que no se haya accedido previamente, se cogen los datos de la ACB
            print('Getting data from the matchevents endpoint...')
            matchFullEvents = getMatchFullEvents(gameID)

            if matchFullEvents is not None:
                for matchFullEvent in matchFullEvents:
                    if matchFullEvent['team'] is not None and matchFullEvent['id_license'] is not None:
                        matchEvent = {
                            'team_id': matchFullEvent['team']['id_team_denomination'],
                            'player_license_id': matchFullEvent['id_license'],
                            'action_time': matchFullEvent['crono'],
                            'action_type': matchFullEvent['id_playbyplaytype']
                        }
                        matchEvents.append(matchEvent)
                        matchEventForDB = matchEvent.copy()
                        matchEventForDB['game_id'] = gameID
                        pbpLeanSerializer = PbPLeanSerializer(data=matchEventForDB)

                        if pbpLeanSerializer.is_valid(raise_exception=True):
                            pbpLeanSerializer.save()
                
                apiCallSerializer = ApiCallSerializer(data={'game_id': gameID, 'endpoint': 'pbp-lean'})

                if apiCallSerializer.is_valid(raise_exception=True):
                    apiCallSerializer.save()
        else:
            # Si se ha accedido previamente, se cogen los datos de la base de datos
            print('Getting data from the database...')
            matchFullEvents = PbPLean.objects.filter(game_id=gameID)

            for matchFullEvent in matchFullEvents:
                matchEvent = {
                    'team_id': matchFullEvent.team_id,
                    'player_license_id': matchFullEvent.player_license_id,
                    'action_time': matchFullEvent.action_time,
                    'action_type': matchFullEvent.action_type
                }
                matchEvents.append(matchEvent)

        return Response(matchEvents)

# Endpoint Game Leaders
class gameLeadersView(APIView):
    # Autenticación básica
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        gameID = self.kwargs['gameID']
        # Consulta si se ha accedido previamente a ese mismo endpoint y partido
        repeatedCall = ApiCall.objects.filter(game_id=gameID, endpoint='game-leaders').count() > 0
        statistics = ('points', 'rebounds')
        response = {
            'home_team_leaders': [],
            'away_team_leaders': []
        }

        if not repeatedCall:
            # En caso de que no se haya accedido previamente, se cogen los datos de la ACB
            print('Getting data from the matchevents endpoint...')
            matchFullEvents = getMatchFullEvents(gameID)
            homePlayers = {}
            awayPlayers = {}

            if matchFullEvents is not None:
                for matchFullEvent in matchFullEvents:
                    if matchFullEvent['team'] is not None and matchFullEvent['id_license'] is not None and matchFullEvent['statistics'] is not None:
                        if matchFullEvent['local']:
                            homePlayers[matchFullEvent['id_license']] = {
                                'name': matchFullEvent['license']['licenseAbbrev'],
                                'points': matchFullEvent['statistics']['points'],
                                'rebounds': matchFullEvent['statistics']['total_rebound']
                            }
                        else:
                            awayPlayers[matchFullEvent['id_license']] = {
                                'name': matchFullEvent['license']['licenseAbbrev'],
                                'points': matchFullEvent['statistics']['points'],
                                'rebounds': matchFullEvent['statistics']['total_rebound']
                            }
                
                if len(matchFullEvents) > 0:
                    for statistic in statistics:
                        homePlayers = collections.OrderedDict(sorted(homePlayers.items(), key=lambda t:t[1][statistic], reverse=True))
                        awayPlayers = collections.OrderedDict(sorted(awayPlayers.items(), key=lambda t:t[1][statistic], reverse=True))
                        homeLeaderID = next(iter(homePlayers))
                        awayLeaderID = next(iter(awayPlayers))
                        response['home_team_leaders'].append({'player_license': homeLeaderID, statistic: homePlayers[homeLeaderID][statistic]})
                        response['away_team_leaders'].append({'player_license': awayLeaderID, statistic: awayPlayers[awayLeaderID][statistic]})
                        gameLeaders = {
                            'game_id': gameID,
                            'statistic_type': statistic,
                            'home_team_leader': homeLeaderID,
                            'home_team_value': homePlayers[homeLeaderID][statistic],
                            'away_team_leader': awayLeaderID,
                            'away_team_value': awayPlayers[awayLeaderID][statistic]
                        }
                        gameLeadersSerializer = GameLeadersSerializer(data=gameLeaders)

                        if gameLeadersSerializer.is_valid(raise_exception=True):
                            gameLeadersSerializer.save()

                    apiCallSerializer = ApiCallSerializer(data={'game_id': gameID, 'endpoint': 'game-leaders'})

                    if apiCallSerializer.is_valid(raise_exception=True):
                        apiCallSerializer.save()
        else:
            # Si se ha accedido previamente, se cogen los datos de la base de datos
            print('Getting data from the database...')
            gameLeaders = GameLeaders.objects.filter(game_id=gameID)
            
            for gameLeadersStat in gameLeaders:
                response['home_team_leaders'].append({'player_license': gameLeadersStat.home_team_leader, gameLeadersStat.statistic_type: gameLeadersStat.home_team_value})
                response['away_team_leaders'].append({'player_license': gameLeadersStat.away_team_leader, gameLeadersStat.statistic_type: gameLeadersStat.away_team_value})

        return Response(response)

# Endpoint Game Biggest Lead
class gameBiggestLeadView(APIView):
    # Autenticación básica
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        gameID = self.kwargs['gameID']
        # Consulta si se ha accedido previamente a ese mismo endpoint y partido
        repeatedCall = ApiCall.objects.filter(game_id=gameID, endpoint='game-biggest-lead').count() > 0
        gameBiggestLead = {
            'home_team': 0,
            'away_team': 0
        }

        if not repeatedCall:
            # En caso de que no se haya accedido previamente, se cogen los datos de la ACB
            print('Getting data from the matchevents endpoint...')
            matchFullEvents = getMatchFullEvents(gameID)
            homeBiggestLead = 0
            awayBiggestLead = 0

            if matchFullEvents is not None:
                for matchFullEvent in matchFullEvents:
                    homeLead = matchFullEvent['score_local'] - matchFullEvent['score_visitor']
                    awayLead = matchFullEvent['score_visitor'] - matchFullEvent['score_local']

                    if homeLead > homeBiggestLead:
                        homeBiggestLead = homeLead
                    
                    if awayLead > awayBiggestLead:
                        awayBiggestLead = awayLead
                
                gameBiggestLead = {
                    'home_team': homeBiggestLead,
                    'away_team': awayBiggestLead
                }

                gameBiggestLead['game_id'] = gameID
                gameBiggestLeadSerializer = GameBiggestLeadSerializer(data=gameBiggestLead)

                if gameBiggestLeadSerializer.is_valid(raise_exception=True):
                    gameBiggestLeadSerializer.save()

                apiCallSerializer = ApiCallSerializer(data={'game_id': gameID, 'endpoint': 'game-biggest-lead'})

                if apiCallSerializer.is_valid(raise_exception=True):
                    apiCallSerializer.save()
                
                del gameBiggestLead['game_id']
        else:
            # Si se ha accedido previamente, se cogen los datos de la base de datos
            print('Getting data from the database...')
            gameBiggestLeadDB = GameBiggestLead.objects.get(game_id=gameID)

            gameBiggestLead = {
                'home_team': gameBiggestLeadDB.home_team,
                'away_team': gameBiggestLeadDB.away_team
            }

        return Response(gameBiggestLead)